package com.lru.cache.store;

public interface Cache<K, V> {

  public void add(K key, V value);

  public V get(K key);

  public boolean exists(K key);

  public void delete(K key);

  public void flush();

  public int size();
}
