package com.lru.cache.store;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LRUCache<K, V> implements Cache<K, V> {

  static class Node<K, V> {

    private V value;
    private K key;
    private Node<K, V> next, prev;

    public Node(K key, V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public String toString() {
      return "Node{" +
          "value=" + value +
          ", key=" + key +
          '}';
    }
  }

  private final int maxSize;
  private Node<K, V> head, tail;

  public LRUCache(int maxSize) {
    this.maxSize = maxSize;
  }

  private Map<K, Node<K, V>> cacheMap = new ConcurrentHashMap<>();

  private void addNodeToTailOfDLL(Node<K, V> node) {
    if (node == null) {
      return;
    }
    if (head == null) {
      head = tail = node;
    } else {
      tail.next = node;
      node.prev = tail;
      tail = node;
    }
  }

  private void removeNodeFromDLL(Node<K, V> node) {
    if (head == node && tail == node) {
      head = tail = null;
    } else if (head == node) {
      head = node.next;
      node.next.prev = null;
    } else if(tail == node){
      tail = tail.prev;
      tail.next = null;
    } else {
      node.prev.next =node.next;
      node.next.prev = node.prev;
    }
  }

  @Override
  public synchronized void add(K key, V value) {
    Node<K, V> node = cacheMap.get(key);
    if (node != null) {
      removeNodeFromDLL(node);
      node.next = node.prev = null;
    } else {
      if(cacheMap.size() >= maxSize){
        cacheMap.remove(head.key);
        removeNodeFromDLL(head);
      }
      node = new Node(key, value);
      cacheMap.put(key, node);
    }
    addNodeToTailOfDLL(node);
  }

  @Override
  public V get(K key) {
    Node<K, V> node = cacheMap.get(key);
    if(node != null){
      return node.value;
    }
    return null;
  }

  @Override
  public boolean exists(K key) {
    return cacheMap.containsKey(key);
  }

  @Override
  public synchronized void delete(K key) {
    Node<K, V> node = cacheMap.get(key);
    if(node != null){
      removeNodeFromDLL(node);
      cacheMap.remove(key);
    }
  }

  @Override
  public void flush() {
    cacheMap.clear();
    head = tail = null;
  }

  @Override
  public int size() {
    return cacheMap.size();
  }
}
