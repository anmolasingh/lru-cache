package com.lru.cache.store;

import static org.junit.jupiter.api.Assertions.*;

import com.lru.cache.store.LRUCache.Node;
import org.junit.Test;

public class LRUCacheTest {

  @Test
  public void integrationTest() {
    LRUCache<String, String> cache = new LRUCache<>(5);
    cache.add("k1", "v1");
    cache.add("k2", "v2");
    cache.add("k3", "v3");
    cache.add("k2", "v2");
    assertEquals(cache.size(), 3);
    assertEquals(cache.exists("k1"), true);
    assertEquals(cache.get("k2"), "v2");
    cache.add("k4", "v4");
    cache.add("k5", "v5");
    cache.add("k6", "v6");
    cache.add("k7", "v7");
    cache.add("k8", "v8");
    assertEquals(cache.size(), 5);
    assertEquals(cache.exists("k1"), false);
    assertEquals(cache.exists("k3"), false);
    assertEquals(cache.exists("k7"), true);
    assertEquals(cache.exists("k8"), true);
    cache.flush();
    assertEquals(cache.size(), 0);
    cache.add("k9","v9");
    assertEquals(cache.size(), 1);
  }
}
